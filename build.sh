#!/bin/bash
set -e

# Download sources
mkdir -p SOURCES
spectool -g -C ./SOURCES SPECS/tinyproxy.spec
# Build RPM
rpmbuild -v --define "%_topdir `pwd`" -bb SPECS/tinyproxy.spec

tree RPMS

# Remove unwanted RPM
rm -f RPMS/x86_64/*-debuginfo-*.rpm
