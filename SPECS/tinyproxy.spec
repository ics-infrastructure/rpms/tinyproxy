%global _hardened_build 1
%define _name tinyproxy
%define version 1.11.1

%define tinyproxy_confdir %{_sysconfdir}/tinyproxy
%define tinyproxy_datadir %{_datadir}/tinyproxy
%define tinyproxy_rundir  /var/run/tiny-proxy
%define tinyproxy_logdir  %{_localstatedir}/log/tinyproxy
%define tinyproxy_user    tinyproxy
%define tinyproxy_group   tinyproxy
%define src_dir tinyproxy

Name:           %{_name}
Version:        %{version}
Release:        0
Summary:        A small, efficient HTTP/SSL proxy daemon
License:        GPLv2+
URL:            https://github.com/tinyproxy
Source0:        https://github.com/tinyproxy/tinyproxy/releases/download/%{version}/%{name}-%{version}.tar.xz
Source1:        https://src.fedoraproject.org/rpms/tinyproxy/raw/rawhide/f/tinyproxy.service
Source2:        https://src.fedoraproject.org/rpms/tinyproxy/raw/rawhide/f/tinyproxy.conf
Source3:        https://src.fedoraproject.org/rpms/tinyproxy/raw/rawhide/f/tinyproxy.logrotate
Source4:        https://src.fedoraproject.org/rpms/tinyproxy/raw/rawhide/f/tinyproxy.tmpfiles
BuildRequires: autoconf asciidoc gcc gcc-c++ make systemd
Buildroot:      %{_tmppath}/%{name}-%{version}-root

%description
tinyproxy is a small, efficient HTTP/SSL proxy daemon that is very useful in a
small network setting, where a larger proxy like Squid would either be too
resource intensive, or a security risk.  

%prep
%setup -q

%build

%configure --sysconfdir=%{_sysconfdir} \
    --enable-reverse \
    --enable-transparent 

make LDFLAGS="%{?__global_ldflags}" CFLAGS="-DNDEBUG $RPM_OPT_FLAGS" %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%{__install} -p -D -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{name}.service
%{__install} -p -D -m 0644 %{SOURCE2} %{buildroot}%{tinyproxy_confdir}/%{name}.conf
%{__install} -p -D -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
%{__install} -p -D -m 0644 %{SOURCE4} %{buildroot}%{_tmpfilesdir}/%{name}.conf
%{__install} -p -d -m 0700 %{buildroot}%{tinyproxy_rundir}
%{__install} -p -d -m 0700 %{buildroot}%{tinyproxy_logdir}

%{__install} -p -d -m 0755 %{buildroot}%{_sbindir}
ln -s ../bin/%{name} %{buildroot}%{_sbindir}/%{name}

%pre
if [ $1 == 1 ]; then
    %{_sbindir}/useradd -c "tinyproxy user" -s /bin/false -r -d %{tinyproxy_rundir} %{tinyproxy_user} 2>/dev/null || :
fi


%post
/bin/systemd-tmpfiles --create %{_tmpfilesdir}/%{name}.conf
%systemd_post %{name}.service
    

%preun
%systemd_preun %{name}.service


%postun
%systemd_postun_with_restart %{name}.service

%files
%doc AUTHORS COPYING README README.md NEWS docs/*.txt
%{_bindir}/%{name}
%{_mandir}/man8/%{name}.8.gz
%{_mandir}/man5/%{name}.conf.5.gz
%{_unitdir}/%{name}.service
%{_tmpfilesdir}/%{name}.conf
%{tinyproxy_datadir}
%dir %{tinyproxy_confdir}
#%ghost %dir %{tinyproxy_rundir}
#%dir %{tinyproxy_logdir}
%config(noreplace) %{tinyproxy_confdir}/%{name}.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%attr(0700,%{tinyproxy_user},%{tinyproxy_group}) %ghost %dir %{tinyproxy_rundir}
%attr(0700,%{tinyproxy_user},%{tinyproxy_group}) %dir %{tinyproxy_logdir}
/usr/sbin/%{name}
/usr/share/doc/tinyproxy/AUTHORS
/usr/share/doc/tinyproxy/NEWS
/usr/share/doc/tinyproxy/README
/usr/share/doc/tinyproxy/README.md
